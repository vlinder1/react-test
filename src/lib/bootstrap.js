import $ from 'jquery';
import React from 'react';

class Test extends React.Component {
	render() {
		return <div>Hello World</div>;
	}
}

export function bootstrap() {
	React.render(<Test />, document.body);
}

